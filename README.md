# PROJECTE-FINAL
# Configuración de Samba4 Active Directory en Linux

Este proyecto describe los pasos necesarios para configurar un servidor Samba4 Active Directory en un sistema Linux.

## Introducción

Samba4 es una implementación de software libre del protocolo de servicios de archivos compartidos de Microsoft Windows. Permite a los sistemas Linux actuar como controladores de dominio compatibles con Active Directory. Esta configuración es útil para entornos que requieren autenticación centralizada y gestión de usuarios.

## Requisitos del Sistema

- Sistema operativo Linux compatible
- Conexión a Internet para la instalación de paquetes
- Recursos de hardware suficientes para las necesidades del dominio

## Instalación de Paquetes

Ejecuta el siguiente comando para instalar los paquetes necesarios:

## Instalación de paquetes necesarios
```
apt-get install -y vim acl attr samba samba-dsdb-modules samba-vfs-modules smbclient winbind libpam-winbind libnss-winbind libpam-krb5 krb5-config krb5-user dnsutils chrony net-tools
```
## Detención y desactivación de servicios
```
systemctl stop samba-ad-dc smbd nmbd winbind
systemctl disable samba-ad-dc smbd nmbd winbind
```
## Copia de seguridad y eliminación de smb.conf
```
cp /etc/samba/smb.conf{,.org}
rm /etc/samba/smb.conf
```
## Creación del dominio con nuestras credenciales
```
sudo samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm=diego.org --domain=diego --function-level=2008_R2 --adminpass="Diego876"
```
## Edición del archivo smb.conf
```
sudo vim /etc/samba/smb.conf
```

# Global parameters
```
[global]
    dns forwarder = 192.168.1.10
    netbios name = AD
    realm = DIEGO.ORG
    server role = active directory domain controller
    workgroup = DIEGO
    idmap_ldb:use rfc2307 = yes
    ldap server require strong auth = no

[sysvol]
    path = /var/lib/samba/sysvol
    read only = No
    create mask = 0700
    directory mask = 0644

[netlogon]
    path = /var/lib/samba/sysvol/diego.org/scripts
    read only = No
    create mask = 0700
    directory mask = 0644
```

## Configuración de krb5.conf
```
sudo mv /etc/krb5.conf{,.org}
sudo ln -s /var/lib/samba/private/krb5.conf /etc/krb5.conf
sudo vim /etc/krb5.conf
```
```
[libdefaults]
    default_realm = DIEGO.ORG
    dns_lookup_realm = false
    dns_lookup_kdc = true

[realms]
DIEGO.ORG = {
    kdc = DC.DIEGO.ORG
    master_kdc = DC.DIEGO.ORG
    admin_server = DC.DIEGO.ORG
    default_domain = diego.org
}

[domain_realm]
    .example.tdl = DIEGO.ORG
    example.tdl = DIEGO.ORG
    AD = DIEGO.ORG
```
## Habilitación del servicio samba-ad-dc

```
sudo systemctl unmask samba-ad-dc
sudo systemctl start samba-ad-dc
sudo systemctl status samba-ad-dc
sudo systemctl enable samba-ad-dc
```
## Creación de usuarios y activación
```
sudo samba-tool user create oscar
```
User: oscar
passwd: Oscar876
```
sudo samba-tool user create pau
```
pau
Pauet876